import Card from './Card.ts';
import { Transaction } from './Transaction.ts';
import { CurrencyEnum } from './CurrencyEnum.ts';

const myCard = new Card();

// Додаємо транзакцію, передаємо екземпляр Transaction
const transaction1 = new Transaction(100, CurrencyEnum.UAH);
const id1 = myCard.AddTransaction(transaction1);
console.log(`Added transaction with ID: ${id1}`);

// Додаємо транзакцію, передаємо кількість та валюту
const id2 = myCard.AddTransaction(200, CurrencyEnum.USD);
console.log(`Added transaction with ID: ${id2}`);

// Знаходимо транзакцію за ID
const foundTransaction = myCard.GetTransaction(id1);
console.log('Found transaction:', foundTransaction);

// Отримуємо баланс по валюті
const balanceUAH = myCard.GetBalance(CurrencyEnum.UAH);
console.log(`Balance in UAH: ${balanceUAH}`);

const balanceUSD = myCard.GetBalance(CurrencyEnum.USD);
console.log(`Balance in USD: ${balanceUSD}`);
