import { CurrencyEnum } from './CurrencyEnum.ts';
import { v4 as uuidv4 } from 'uuid';

export class Transaction {
  public Id: string;
  public Amount: number;
  public Currency: CurrencyEnum;

  constructor(amount: number, currency: CurrencyEnum) {
    this.Id = uuidv4();
    this.Amount = amount;
    this.Currency = currency;
  }
}
