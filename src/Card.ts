import { Transaction } from './Transaction.ts';
import { CurrencyEnum } from './CurrencyEnum.ts';

class Card {
  private transactions: Transaction[] = [];

  // AddTransaction(transaction: Transaction): string {
  //   this.transactions.push(transaction);
  //   return transaction.Id;
  // }

  // AddTransaction(amount: number, currency: CurrencyEnum): string;
  // AddTransaction(transactionOrAmount: Transaction | number, currency?: CurrencyEnum): string {
  //   if (typeof transactionOrAmount === 'number' && currency !== undefined) {
  //     const transaction = new Transaction(transactionOrAmount, currency);
  //     return this.AddTransaction(transaction);
  //   } else if (transactionOrAmount instanceof Transaction) {
  //     return this.AddTransaction(transactionOrAmount);
  //   }
  //   throw new Error('Invalid arguments');
  // }
  // Overload signatures
  AddTransaction(transaction: Transaction): string;
  AddTransaction(amount: number, currency: CurrencyEnum): string;

  // Implementation signature
  AddTransaction(
    transactionOrAmount: Transaction | number,
    currency?: CurrencyEnum,
  ): string {
    if (typeof transactionOrAmount === 'number' && currency !== undefined) {
      const transaction = new Transaction(transactionOrAmount, currency);
      return this.AddTransaction(transaction); // Recursion is used here
    } else if (transactionOrAmount instanceof Transaction) {
      this.transactions.push(transactionOrAmount);
      return transactionOrAmount.Id;
    }
    throw new Error('Invalid arguments');
  }

  GetTransaction(id: string): Transaction | null {
    return this.transactions.find(transaction => transaction.Id === id) || null;
  }

  GetBalance(currency: CurrencyEnum): number {
    return this.transactions
      .filter(transaction => transaction.Currency === currency)
      .reduce((acc, transaction) => acc + transaction.Amount, 0);
  }
}

export default Card;
